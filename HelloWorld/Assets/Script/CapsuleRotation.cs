using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
       [SerializeField]
        private Vector3 Rotation; // Estos serían los ejes de Rotación
        public float speedRotation; // Esta es la velocidad de movimiento




    // Update is called once per frame
    void Update() // Esto sería la acotación de los valores de rotación al valor unitario [-1,1]
    {
        Rotation = ClampVector3(Rotation);

        transform.Rotate(Rotation * (speedRotation * Time.deltaTime)); // Rotación del componente transform en base al tiempo
    }

    /*
    * Función auxiliar que permite acotar los valores de las componentes 
    * de un Vector3 entre -1 y 1.
    */
    public static Vector3 ClampVector3(Vector3 target)
    {
        float clampedX = Mathf.Clamp(target.x, -1f, 1f);
        float clampedY = Mathf.Clamp(target.y, -1f, 1f); 
        float clampedZ = Mathf.Clamp(target.z, -1f, 1f);

        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);

        return result;      
    }
}
